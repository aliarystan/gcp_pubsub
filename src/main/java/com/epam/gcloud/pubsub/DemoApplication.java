package com.epam.gcloud.pubsub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	/*@Bean
	public CommandLineRunner getCommandLiner() {
		return (args) -> {
			String PROJECT_ID = ServiceOptions.getDefaultProjectId() ;
			String SUBSCRIPTION_ID = "topic1-sub2" ;
			ProjectSubscriptionName subscriptionName = ProjectSubscriptionName.of(PROJECT_ID, SUBSCRIPTION_ID) ;
			Subscriber subscriber = null ;
			Log log = LogFactory.getLog(DemoApplication.class) ;
			log.info(String.format("Project, %s", PROJECT_ID) );
			try {
				subscriber = Subscriber.newBuilder(subscriptionName, new GMessageReceiver()).build() ;
				subscriber.startAsync().awaitRunning();
				subscriber.awaitTerminated();

			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
		} ;
	}*/

}
