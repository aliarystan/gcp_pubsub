package com.epam.gcloud.pubsub.api;

import com.epam.gcloud.pubsub.service.OrderingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/order")
public class OrderingController {

    @Autowired
    private OrderingService service;

    /**
     * ORDER	given: messages: 1-300;
     * when: 1, 2 failed, 3,4… success;
     * question: Как себя будет вести очередь и сколько раз будет выполнятся успешные и неуспешные сообщения?
     * result: сообщения деляться на юлок по 9 сообщений, и если ошибка в блоке, то придет этот блок.
     * ...
     * message: {order=1, counter=1, failed=true}
     * message: {order=1, counter=2, failed=true}
     * message: {order=1, counter=3, failed=false}
     * message: {order=1, counter=4, failed=false}
     * message: {order=1, counter=5, failed=false}
     * message: {order=1, counter=6, failed=false}
     * message: {order=1, counter=7, failed=false}
     * message: {order=1, counter=8, failed=false}
     * message: {order=1, counter=9, failed=false}
     * ...
     */
    @GetMapping("/case_1")
    public String firstTwoFails() {
        return service.firstTwoFails();
    }

    /**
     * ORDER	given: messages: 1-300;
     * when: 999, 300 failed, 1,2… success;
     * question: Как себя будет вести очередь и сколько
     * раз будет выполнятся успешные и неуспешные сообщения?
     * (С кокого по какой перевызовется несколько раз?)
     * result: 8 сообщений до
     * ...
     * message: {order=1, counter=992, failed=false}
     * message: {order=1, counter=993, failed=false}
     * message: {order=1, counter=994, failed=false}
     * message: {order=1, counter=995, failed=false}
     * message: {order=1, counter=996, failed=false}
     * message: {order=1, counter=997, failed=false}
     * message: {order=1, counter=998, failed=false}
     * message: {order=1, counter=999, failed=true}
     * message: {order=1, counter=300, failed=true}
     * ...
     */
    @GetMapping("/case_2")
    public String lastTwoFails() {
        return service.lastTwoFails();
    }

    /**
     * ORDER	given: messages: 1-300;
     * when: 499, 500 failed, 1,2… success;
     * question: Как себя будет вести очередь и сколько
     * раз будет выполнятся успешные и неуспешные сообщения?
     * (С кокого по какой перевызовется несколько раз?)
     * result: repeat
     *
     * message: {order=1, counter=497, failed=false}
     * message: {order=1, counter=498, failed=false}
     * message: {order=1, counter=499, failed=false}
     * message: {order=1, counter=500, failed=false}
     * message: {order=1, counter=501, failed=false}
     * message: {order=1, counter=502, failed=false}
     * message: {order=1, counter=503, failed=false}
     * message: {order=1, counter=504, failed=true}
     * message: {order=1, counter=505, failed=true}
     */
    @GetMapping("/case_3")
    public String middleTwoFails() {
        return service.middleTwoFails();
    }

    /**
     * ORDER	given: messages: 1-300;
     * when: 1, 2 failed, 1,2… success;
     * question: Как себя будет вести очередь и сколько
     * раз будет выполнятся успешные и неуспешные сообщения?
     * (С Retry after exponential backoff delay)
     *
     * приходящие сообщения выглядят как и при Retry immediately
     * но просто дольше вызываются
     */
    @GetMapping("/case_4")
    public String firstTwoFailsWithBackoffDelay() {
        return service.firstTwoFailsWithBackoffDelay();
    }

    /**
     * ORDER	given: messages: 1-300;
     * when: 1, 2 failed, 1,2… success;
     * question: Каково поведение если есть 2 хендлера на 1 сабскрипшн с ордерингом?
     */
    @GetMapping("/case_5")
    public String firstTwoFailsWithTwoHandlers() {
        return service.firstTwoFailsWithTwoHandlers();
    }

    /**
     * ORDER	given: messages: 1-20, потом приходят 3 невалидных,
     * потом через каждые 5 милисек по 2 валидных сообщения;
     * when: 20 msg was came,
     * then after 2 sec recieved 21, 22 and 23 is failing,
     * and after 5-10 millis others comming is successfull;
     * question: Что будет с правильными сообщениями которые будут прихрдить после падающих
     * answer: if the gap between messages more then 5 millis then only failed will come to handler
     * result: repeat
     * {order=1, counter=21, failed=true}
     * {order=1, counter=22, failed=true}
     * {order=1, counter=23, failed=true}
     */
    @GetMapping("/case_6")
    public String invalidMessagesBetweenValidWithDelayFiveMillis() {
        return service.invalidMessagesBetweenValid(10);
    }

    /**
     * ORDER	given: messages: 1-20, потом приходят 3 невалидных,
     * потом через каждые 5 милисек по 2 валидных сообщения;
     * when: 20 msg was came,
     * then after 3 sec recieved 21, 22 and 23 is failing,
     * and after 1 millis others comming is successfull;
     * question: Что будет с правильными сообщениями которые будут прихрдить после падающих
     *
     * answer: if first 20 was came then 3 failed with 4 valid will came together
     * result: repeat
     * {order=1, counter=21, failed=true}
     * {order=1, counter=22, failed=true}
     * {order=1, counter=23, failed=true}
     * ...
     * {order=1, counter=27, failed=false}
     */
    @GetMapping("/case_7")
    public String invalidMessagesBetweenValidWithDelayOneMillis() {
        return service.invalidMessagesBetweenValid(1);
    }

    /**
     * ORDER	given: messages: 1-20, потом приходят 3 невалидных,
     * потом через каждую секунду по 2 валидных сообщения;
     * when: 20 msg was came,
     * then after 3 sec recieved 21, 22 and 23 is failing,
     * and others comming is successfull;
     * question: Что будет с правильными сообщениями которые будут прихрдить после падающих (С Retry after exponential backoff delay)
     */
    @GetMapping("/case_8")
    public String invalidMessagesAfterValidWithBackoffDelay() {
        return service.invalidMessagesAfterValidWithBackoffDelay();
    }

    /**
     * ORDER	given: messages: 1-300;
     * when: every even message failed;
     * question:
     * Посмотреть мониторинг ОРДЕРА.
     */
    @GetMapping("/case_9")
    public String everyEvenMessageFailed() {
        return service.everyEvenMessageFailed();
    }

    /**
     * ORDER given: messages: 1-300 with order 1 and 1-300 with order 2;
     * when: every even message failed for order 1 and all success for order 2;
     * question:
     * Посмотреть мониторинг ОРДЕРА.
     * Сколько сообщений придет с ордерингом 2
     * 
     * случай 1
     * пришли 9 С ОРД1
     * потом 9 С ОРД2
     * те же 9 сообщений ОРД1
     * с 10 по 216 С ОРД2
     * те же 9 сообщений ОРД1
     * с 10 по 300 С ОРД2
     * далле все время 9 С ОРД1
     *
     * кейс 2
     * сообщения чередуются
     * 9 С ОРД1 невалид
     * от 9 до n валидных с ОРД1
     */
    @GetMapping("/case_10")
    public String everyEvenMessageFailedForFirstOrder() {
        return service.everyEvenMessageFailedForFirstOrder();
    }

    /**
     * ORDER given: messages: 1-300 with order 1 and 1-300 with order 2;
     * when: every even message failed for order 1 and all success for order 2;
     * and messages came every second parallel
     * question:
     * Сколько сообщений придет с ордерингом 2
     *
     * сообщения орд2 придут все, при этом чередуясь с рорд1
     * из орд 1 будут постоянно приходить сообшения с 1 по 6-9
     */
    @GetMapping("/case_11")
    public String everyEvenMessageFailedForFirstOrderParallel() {
        return service.everyEvenMessageFailedForFirstOrderParallel();
    }

    /**
     * ORDER given: orders: 5 with messages: 300;
     * when: every even message failed for order 2 and success for others;
     * and messages came every second parallel
     * question:
     * Сколько сообщений придет
     *
     * Перыве сообщения приходят чередуясь при этом несколько валидных сообщений пришли повторно
     * в ордирах где все сообщения правильные, далее сообщения приходят в таком порядке
     * {order=2, counter=2, failed=true}
     * {order=2, counter=2, failed=true}
     * {order=4, counter=8, failed=false}
     * {order=3, counter=8, failed=false}
     * {order=1, counter=8, failed=false}
     * {order=5, counter=8, failed=false}
     * {order=2, counter=2, failed=true}
     * {order=2, counter=2, failed=true}
     * {order=5, counter=9, failed=false}
     * {order=1, counter=9, failed=false}
     * {order=4, counter=9, failed=false}
     * {order=3, counter=9, failed=false}
     * ...
     */
    @GetMapping("/case_12")
    public String everyEvenMessageFailedForSecondOrderParallel() {
        return service.everyEvenMessageFailedForSecondOrderParallel();
    }

}