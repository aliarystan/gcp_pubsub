package com.epam.gcloud.pubsub.api;

import com.epam.gcloud.pubsub.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Autowired
    TestService testService;


//    что если сообщения пришли но потом приходит еще одно, но не валидное?
//    если отработает - значит все сообщения от пользователя будут тригерить предудущие в течении их жизни
//    как все 10 сообщений придут запустить еще подписчика
    @GetMapping("/send")
    public String sendEvent() {
        testService.sendEvent();
        return "ok";
    }

}
