package com.epam.gcloud.pubsub.service;

import com.epam.gcloud.pubsub.event.TestEvent;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Service
public class OrderingService {

    private static final int MESSAGES = 300;

    @Autowired
    private PublishingProducer producer;

    public String firstTwoFails() {
        sendAllEvents(1, List.of(1, 2));
        return "ok";
    }

    public String lastTwoFails() {
        sendAllEvents(1, List.of(999, 1000));
        return "ok";
    }

    public String middleTwoFails() {
        sendAllEvents(1, List.of(499, 500));
        return "ok";
    }

    public String firstTwoFailsWithBackoffDelay() {
        return "false";
    }

    public String firstTwoFailsWithTwoHandlers() {
        return "false";
    }

    @SneakyThrows
    public String invalidMessagesBetweenValid(int gapMillis) {
        sendEvents(1, 1,20, false);
        Thread.sleep(3000);
        sendEvents(1, 21,23, true);
        Thread.sleep(gapMillis);
        for (int i = 24; i < MESSAGES; i+=2) {
            int twoTimes = i + 1;
            sendEvents(1, i, twoTimes, false);
            Thread.sleep(gapMillis);
        }
        return "ok";
    }

    public String invalidMessagesAfterValidWithBackoffDelay() {
        return "false";
    }

    public String everyEvenMessageFailed() {
        sendEventsWhenEveryEvenFailed(1);
        return "ok";
    }

    public String everyEvenMessageFailedForFirstOrder() {
        sendEventsWhenEveryEvenFailed(1);
        sendAllValidEvents(2);
        return "ok";
    }

    public String everyEvenMessageFailedForFirstOrderParallel() {
        final ExecutorService executorService = Executors.newFixedThreadPool(2);
        Runnable orderOne = () -> sendEventsWhenEveryEvenFailedWaitSecond(1);
        Runnable orderTwo = () -> sendAllValidEventsWaitSecond(2);
        List.of(orderOne, orderTwo).forEach(executorService::execute);
        return "ok";
    }

    public String everyEvenMessageFailedForSecondOrderParallel() {
        final ExecutorService executorService = Executors.newFixedThreadPool(5);
        Runnable orderOne = () -> sendAllValidEventsWaitSecond(1);
        Runnable orderTwo = () -> sendEventsWhenEveryEvenFailedWaitSecond(2);
        Runnable orderThree = () -> sendAllValidEventsWaitSecond(3);
        Runnable orderFour = () -> sendAllValidEventsWaitSecond(4);
        Runnable orderFive = () -> sendAllValidEventsWaitSecond(5);
        List.of(orderOne, orderTwo, orderThree, orderFour, orderFive).forEach(executorService::execute);
        executorService.shutdown();
        return "ok";
    }

    private void sendAllEvents(int order, List<Integer> failed) {
        for (int i = 1; i <= MESSAGES; i++) {
            synchronized (this) {
                produceEvent(order, i, failed.contains(i));
            }
        }
    }

    @SneakyThrows
    private void sendAllValidEventsWaitSecond(int order) {
        for (int i = 1; i <= MESSAGES; i++) {
            synchronized (this) {
                produceEvent(order, i, false);
                wait(1000);
            }
        }
    }

    private void sendAllValidEvents(int order) {
        for (int i = 1; i <= MESSAGES; i++) {
            synchronized (this) {
                produceEvent(order, i, false);
            }
        }
    }

    private void sendEventsWhenEveryEvenFailed(int order) {
        for (int i = 1; i <= MESSAGES; i++) {
            synchronized (this) {
                produceEvent(order, i, i % 2 == 0);
            }
        }
    }

    @SneakyThrows
    private void sendEventsWhenEveryEvenFailedWaitSecond(int order) {
        for (int i = 1; i <= MESSAGES; i++) {
            synchronized (this) {
                produceEvent(order, i, i % 2 == 0);
                wait(1000);
            }
        }
    }

    private void sendEvents(int order, int from, int to, boolean failed) {
        for (int i = from; i <= to; i++) {
            synchronized (this) {
                produceEvent(order, i, failed);
            }
        }
    }

    private void produceEvent(int id, int counter, boolean failed) {
        TestEvent event = new TestEvent();
        event.setOrder(id);
        event.setCounter(counter);
        event.setFailed(failed);
        producer.produce(event, id);
    }
}
