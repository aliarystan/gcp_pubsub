package com.epam.gcloud.pubsub.service;

import com.epam.gcloud.pubsub.event.TestEvent;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Random;
import java.util.UUID;

@Slf4j
@Service
public class TestService {
    @Autowired
    private PublishingProducer producer;


    public void sendEvent() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                synchronized (this) {
                    produceEvent(i, j);
                }
            }
        }
    }

    @SneakyThrows
    private void produceEvent(int id, int counter) {
        TestEvent event = new TestEvent();
        event.setOrder(id);
        event.setCounter(counter);
        producer.produce(event, id);
    }

    public void handle(TestEvent message, Map<String, String> headers) {
        synchronized (this) {
            System.out.println("message: " + message);
        }
    }

}
