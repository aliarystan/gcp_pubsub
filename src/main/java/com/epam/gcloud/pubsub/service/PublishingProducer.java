package com.epam.gcloud.pubsub.service;

import com.epam.gcloud.pubsub.config.MessageSerializer;
import com.epam.gcloud.pubsub.event.GenericEvent;
import lombok.extern.slf4j.Slf4j;
import ng.platform.messaging.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

import static ng.platform.messaging.Headers.CREATED;
import static ng.platform.messaging.Headers.MESSAGE_ID;
import static ng.platform.messaging.Headers.TYPE;

@Slf4j
@Service
public class PublishingProducer {

    @Qualifier("signals-pubsub-producer")
    @Autowired
    Producer producer;

    @Autowired
    MessageSerializer serializer;

    public void produce(GenericEvent event, Integer id) {
        producer.produce(serializer.serialize(event), generateHeaders(id));
    }

    private Map<String, String> generateHeaders(Integer id) {
        Map<String, String> headers = new HashMap<>();
        headers.put(MESSAGE_ID, id.toString());
        headers.put(CREATED, Long.toString(Timestamp.from(OffsetDateTime.now().toInstant()).getTime()));
        headers.put(TYPE, "Test.Event");
        return headers;
    }
}
