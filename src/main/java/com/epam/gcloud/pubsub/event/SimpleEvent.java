package com.epam.gcloud.pubsub.event;

public interface SimpleEvent {
    String getEventType();
}
