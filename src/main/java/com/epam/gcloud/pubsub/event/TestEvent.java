package com.epam.gcloud.pubsub.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestEvent  implements GenericEvent {
    private static final String TYPE = "Test.Event";

    private int order;
    private int counter;
    private boolean failed;

    public static String getEventType() {
        return TYPE;
    }

    @Override
    public String toString() {
        return "{" +
                "order=" + order +
                ", counter=" + counter +
                ", failed=" + failed +
                '}';
    }
}