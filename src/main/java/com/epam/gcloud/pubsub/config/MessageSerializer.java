package com.epam.gcloud.pubsub.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import ng.platform.messaging.Deserializer;
import ng.platform.messaging.Serializer;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MessageSerializer implements Serializer, Deserializer {

    private final ObjectMapper mapper;

    @SneakyThrows
    @Override
    public <T> T deserialize(String value, Class<T> type) {
        return mapper.readValue(value, type);
    }

    @SneakyThrows
    public String serialize(Object value) {
        return mapper.writeValueAsString(value);
    }

}
