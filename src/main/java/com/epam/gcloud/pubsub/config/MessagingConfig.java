package com.epam.gcloud.pubsub.config;

import com.epam.gcloud.pubsub.listener.DefaultListener;
import com.epam.gcloud.pubsub.listener.GenericListener;
import com.epam.gcloud.pubsub.listener.MessagingExceptionHandler;
import com.epam.gcloud.pubsub.properties.PubSubProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import ng.platform.messaging.Consumer;
import ng.platform.messaging.Deserializer;
import ng.platform.messaging.MessageLogger;
import ng.platform.messaging.Producer;
import ng.platform.messaging.Serializer;
import ng.platform.messaging.pubsub.PubSubConsumer;
import ng.platform.messaging.pubsub.PubSubConsumerConfig;
import ng.platform.messaging.pubsub.PubSubProducerConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;
import java.util.List;

import static ng.platform.messaging.Headers.TYPE;

@Configuration
@RequiredArgsConstructor
public class MessagingConfig {
    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${message.loggerLevel:NONE}")
    private String messageLogLevel;

    private final MessageSerializer messageSerializer;


    @Configuration
    public class DefaultSerializer implements Serializer, Deserializer {
        private final ObjectMapper mapper;

        public DefaultSerializer(ObjectMapper mapper) {
            this.mapper = mapper;
        }

        @SneakyThrows
        @Override
        public <T> T deserialize(String value, Class<T> type) {
            return mapper.readValue(value, type);
        }

        @SneakyThrows
        public String serialize(Object value) {
            return mapper.writeValueAsString(value);
        }
    }

    @SneakyThrows
    @Primary
    @Bean("signals-pubsub-producer")
    public Producer pubsubProducer(PubSubProperties properties) {
        SignalsPubSubProducer producer;
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.registerModule(new JavaTimeModule());
        xmlMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        DefaultSerializer defaultSerializer = new DefaultSerializer(xmlMapper);


        PubSubProducerConfig config = new PubSubProducerConfig();
        config.setProducerId(applicationName);
        config.setProject(properties.getGcpProjectId());
        config.setTopic(properties.getProducer().getTopic());

        config.setSerializer(defaultSerializer);

        config.setLogLevel(MessageLogger.Level.valueOf(messageLogLevel));
        producer = new SignalsPubSubProducer(config);


        return producer;
    }

    @SneakyThrows
    @Bean("pubsub-consumer")
    public Consumer pubsubConsumer(PubSubProperties properties,
                                   MessagingExceptionHandler handler,
                                   DefaultListener defaultListener,
                                   List<GenericListener> messageHandlerList) {
        PubSubConsumer consumer;


        PubSubConsumerConfig config = new PubSubConsumerConfig();
        config.setProject(properties.getGcpProjectId());
        config.setSubscription(properties.getConsumer().getSubscription());
        config.setThreads(5);
        config.setExceptionHandler(handler);
        config.setDeserializer(messageSerializer);
        config.setDefaultMessageHandler(defaultListener);

        config.setLogLevel(MessageLogger.Level.valueOf(messageLogLevel));
        consumer = new PubSubConsumer(config);


        messageHandlerList.forEach(typedMessageHandler -> consumer.addMessageHandler(
            headers -> StringUtils.contains(headers.get(TYPE), typedMessageHandler.getType()),
            typedMessageHandler,
            typedMessageHandler.getClazz())
        );
        return consumer;
    }


    @Configuration
    public static class ConsumerInitializer {
        @Autowired
        @Qualifier("pubsub-consumer")
        private Consumer pubsubConsumer;

        @PostConstruct
        private void init() {
            pubsubConsumer.listen();
        }
    }
}
