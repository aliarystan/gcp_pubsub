package com.epam.gcloud.pubsub.config;

import ng.platform.messaging.pubsub.PubSubEmulatorProducerConfig;
import ng.platform.messaging.pubsub.PubSubProducer;
import ng.platform.messaging.pubsub.PubSubProducerConfig;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

import static ng.platform.messaging.Headers.CREATED;
import static ng.platform.messaging.Headers.MESSAGE_ID;
import static ng.platform.messaging.Headers.TYPE;

public class SignalsPubSubProducer extends PubSubProducer {

    public SignalsPubSubProducer(PubSubProducerConfig config) {
        super(config);
    }


    public SignalsPubSubProducer(PubSubEmulatorProducerConfig config) {
        super(config);
    }

    @Override
    public void produce(String payload, Map<String, String> headers) {
        if (headers == null) {
            throw new IllegalArgumentException("headers can't be null");
        }

        String messageId = headers.get(MESSAGE_ID);
        String createdAt = headers.get(CREATED);
        String type = headers.get(TYPE);


        if (StringUtils.isBlank(messageId)) {
            throw new IllegalArgumentException("messageId header can't be blank");
        }
        if (StringUtils.isBlank(createdAt)) {
            throw new IllegalArgumentException("createdAt header can't be blank");
        }
        if (StringUtils.isBlank(type)) {
            throw new IllegalArgumentException("type header can't be blank");
        }

        produce(payload, headers, messageId);
    }
}