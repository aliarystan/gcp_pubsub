package com.epam.gcloud.pubsub.listener;

import lombok.extern.slf4j.Slf4j;
import ng.platform.messaging.ExceptionHandler;
import ng.platform.messaging.Meta;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
public class MessagingExceptionHandler implements ExceptionHandler {

    @Override
    public boolean handle(Exception exception) {
        log.error("Message processing error", exception);
        return false;
    }

}
