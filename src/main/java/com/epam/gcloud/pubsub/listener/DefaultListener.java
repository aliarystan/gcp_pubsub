package com.epam.gcloud.pubsub.listener;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ng.platform.messaging.DefaultMessageHandler;
import ng.platform.messaging.Meta;
import org.springframework.stereotype.Service;

import java.util.Map;

@Slf4j
@Service
@AllArgsConstructor
public class DefaultListener implements DefaultMessageHandler {

    @SneakyThrows
    @Override
    public boolean handle(String payload, Map<String, String> map, Meta meta) {
        return true;
    }
}

