package com.epam.gcloud.pubsub.listener;


import com.epam.gcloud.pubsub.event.GenericEvent;
import ng.platform.messaging.TypedMessageHandler;

import java.lang.reflect.ParameterizedType;

public abstract class GenericListener<T extends GenericEvent> implements TypedMessageHandler<T> {
    private final Class<T> clazz = (Class<T>) ((ParameterizedType) getClass()
        .getGenericSuperclass())
        .getActualTypeArguments()[0];

    public Class<T> getClazz() {
        return clazz;
    }

    public abstract String getType();
}
