package com.epam.gcloud.pubsub.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "application.pubsub")
public class PubSubProperties {
    private String gcpProjectId;
    private String emulatorHost;
    private Integer emulatorPort;
    private Consumer consumer;
    private Producer producer;

    @Data
    public static class Consumer {
        private String topic;
        private String subscription;
    }

    @Data
    public static class Producer {
        private String topic;
    }
}
