package com.epam.gcloud.pubsub.handler.ordering;

import com.epam.gcloud.pubsub.event.TestEvent;
import com.epam.gcloud.pubsub.listener.GenericListener;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;


@Slf4j
@Service
public class OrderingHandler extends GenericListener<TestEvent> {

    private static final AtomicInteger attempt = new AtomicInteger();

    @Override
    public String getType() {
        return TestEvent.getEventType();
    }

    @SneakyThrows
    @Override
    public boolean handle(TestEvent message, Map<String, String> headers) {
        if (attempt.getAndIncrement() < 1) {
            Thread.sleep(3000);
        }
        System.out.println(message);
        return !message.isFailed();
    }
}
