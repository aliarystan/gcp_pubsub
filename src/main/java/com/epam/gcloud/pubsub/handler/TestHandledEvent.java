package com.epam.gcloud.pubsub.handler;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.epam.gcloud.pubsub.event.GenericEvent;
import lombok.Data;

import java.util.UUID;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TestHandledEvent implements GenericEvent {
    private static final String TYPE = "Test.Event";

    private UUID id;
    private String name;
    private Long balance;

    public static String getEventType() {
        return TYPE;
    }

}
