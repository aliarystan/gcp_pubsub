package com.epam.gcloud.pubsub.handler;

import com.epam.gcloud.pubsub.listener.GenericListener;
import com.epam.gcloud.pubsub.event.TestEvent;
import com.epam.gcloud.pubsub.service.TestService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class TestHandler {

    @Autowired
    private TestService testService;

    private final AtomicInteger counter = new AtomicInteger();

    public String getType() {
        return TestEvent.getEventType();
    }

//  если нет ордеринг кея попадет ли в дедлетер?
//  как получить инфу из дедлетера куда то
    @SneakyThrows
    public boolean handle(TestEvent message, Map<String, String> headers) {
//        if (counter.get() < 1) {
//            Thread.sleep(5000);
//        }
//        case 2
//        если ошибка в 1с то 2с будет тригерить redeliver
        if (message.getOrder() == 0 && message.getCounter() >= 1) {
            counter.incrementAndGet();
            System.out.println("TRY to execute order 0, message: " + message);
            return false;
        }

//        if (message.getOrder() == 0 && (message.getCounter() > 3 && message.getCounter() < 6)) {
////            counter.incrementAndGet();
//            System.out.println("TRY to execute order 0, message: " + message);
//            Thread.sleep(2000);
//            return false;
//        }
//        case 1
//        if (message.getOrder() == 0 && message.getCounter() < 2 && counter.get() < 3) {
//            final int i = counter.incrementAndGet();
//            System.out.println("try to execute: " + message + ", attempt: " + i);
//            return false;
//        }
        testService.handle(message, headers);
        return true;
    }
}
